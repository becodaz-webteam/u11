
// *
// * Erdmann & Freunde
// * SOLO Contao Theme
// * erdmann-freunde.de/themes/solo/
// *

// object-fit fallback for IE and EDGE
// * if the browser doesn't support object-fit, the script will add the image
//   as background-image.
//
// * Thanks to Primož Cigler & Luuk Lamers. For more information, see
//   https://medium.com/@primozcigler/neat-trick-for-css-object-fit-fallback-on-edge-and-other-browsers-afbc53bbb2c3#.kbdhniq66
// --------------------------------------------------

function objectFitElement(elementClass) {

  elementClass = typeof(elementClass) !== 'undefined' ? elementClass : '.image--object-fit .image_container';

  function hasClass(el, cls) {
    if (!el.className) {
      return false;
    } else {
        var newElementClass = ' ' + el.className + ' ';
        var newClassName = ' ' + cls + ' ';
        return newElementClass.indexOf(newClassName) !== -1;
    }
  }

  function addClass(el, className) {
  	if (el.classList) {
      el.classList.add(className);
    }
  	else if (!hasClass(el, className)) {
      el.className += ' ' + className;
    }
  }

  var imgContainers, len;

  if('objectFit' in document.documentElement.style === false) {

  	imgContainers = document.querySelectorAll(elementClass);
  	len = imgContainers.length;

  	for (var i = 0; i < len; i++) {
  		var $container = imgContainers[i],
  			imgUrl = $container.querySelector('img').getAttribute('src');
  		if (imgUrl) {
  			$container.style.backgroundImage = 'url(' + imgUrl + ')';
  			addClass($container, 'compat-object-fit');
  		}
  	}
  }
}
