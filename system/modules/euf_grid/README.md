# Erdmann & Freunde Grid-Erweiterung für Contao

Mit **euf_grid** kannst du Content-Elemente in Contao an einem 12-spaltigen Grid ausrichten.

Wir empfehlen dir, die Erweiterung in Verbindung mit unserem Contao Theme SOLO zu verwenden. Mehr Infos: http://erdmann-freunde.de/themes/solo/

## Was die Erweiterung macht:

Nach der Installation werden den Content-Elementen 2 neue Felder für die Eingabe von Grid-Klassen und weiteren Optionen hinzugefügt. Außerdem stehen weitere Content-Elemente zur Verschachtelung in Reihen und Spalten zur Verfügung.

**Neu in 2.0**: Die Erweiterung bringt nun Standard CSS-Grid-Anweisungen mit, die im Seitenlayout aktiviert werden können.
