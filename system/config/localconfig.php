<?php

### INSTALL SCRIPT START ###
$GLOBALS['TL_CONFIG']['licenseAccepted'] = true;
$GLOBALS['TL_CONFIG']['installPassword'] = '$2y$10$LS2S6A/hFZ0cAtHDHgUHxO1r9ZJ.0vDAR42FezCgcmy51auLOH0se';
$GLOBALS['TL_CONFIG']['encryptionKey'] = '8113425a02653c956775a487f35b9534';
$GLOBALS['TL_CONFIG']['dbDriver'] = 'MySQL';
$GLOBALS['TL_CONFIG']['dbHost'] = 'localhost';
$GLOBALS['TL_CONFIG']['dbUser'] = 'root';
$GLOBALS['TL_CONFIG']['dbPass'] = 'citacita';
$GLOBALS['TL_CONFIG']['dbDatabase'] = 'u11';
$GLOBALS['TL_CONFIG']['dbPconnect'] = false;
$GLOBALS['TL_CONFIG']['dbCharset'] = 'UTF8';
$GLOBALS['TL_CONFIG']['dbPort'] = 3306;
$GLOBALS['TL_CONFIG']['dbSocket'] = '';
$GLOBALS['TL_CONFIG']['maintenanceMode'] = false;
$GLOBALS['TL_CONFIG']['inactiveModules'] = 'a:1:{i:0;s:10:"repository";}';
$GLOBALS['TL_CONFIG']['installCount'] = 0;
$GLOBALS['TL_CONFIG']['latestVersion'] = '3.5.28';
$GLOBALS['TL_CONFIG']['adminEmail'] = 'office@becoda.at';
$GLOBALS['TL_CONFIG']['gzipScripts'] = true;
// $GLOBALS['TL_CONFIG']['debugMode'] = true;
// $GLOBALS['TL_CONFIG']['displayErrors'] = true;
$GLOBALS['TL_CONFIG']['folderUrl'] = true;
$GLOBALS['TL_CONFIG']['gdMaxImgWidth'] = 30000;
$GLOBALS['TL_CONFIG']['gdMaxImgHeight'] = 300000;
$GLOBALS['TL_CONFIG']['maxFileSize'] = 20480000;
### INSTALL SCRIPT STOP ###
