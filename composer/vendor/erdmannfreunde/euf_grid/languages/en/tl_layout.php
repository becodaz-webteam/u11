<?php

/**
 * @package   EuF-Grid
 * @author    Sebastian Buck
 * @license   LGPL
 * @copyright Erdmann & Freunde
 */


/**
 * CSS Framework Name
 */
$GLOBALS['TL_LANG']['tl_layout']['../../../system/modules/euf_grid/assets/euf_grid.css'] = 'EuF-Grid';
